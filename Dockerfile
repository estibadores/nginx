FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get -qy install nginx-full ssl-cert && \
    apt-get clean

COPY nginx.conf /etc/nginx/nginx.conf

# Basic nginx directories
RUN mkdir -p /var/lib/nginx/body && \ 
    mkdir -p /var/lib/nginx/proxy && \
    mkdir -p /var/lib/nginx/fastcgi && \
    mkdir -p /var/lib/nginx/scgi && \
    mkdir -p /var/lib/nginx/uwsgi
    
# Logging to stdout
RUN ln -sf /proc/self/fd /dev/
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

# PID nginx for non-root users
RUN mkdir -p /run/nginx && \
    chmod 777 /run/nginx && \
    chmod -R 777 /var/lib/nginx

# Use port 8080
RUN sed -i 's/80/8080/g' /etc/nginx/sites-enabled/default

EXPOSE 8080

ENTRYPOINT ["/usr/sbin/nginx"]
