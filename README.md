# Nginx

Simple Nginx web server, standalone, non-root user

# Related links
* https://newbedev.com/have-nginx-access-log-and-error-log-log-to-stdout-and-stderr-of-master-process
* https://docs.nginx.com/nginx/admin-guide/monitoring/logging/
* https://nginx.org/en/docs/ngx_core_module.html
* https://nginx.org/en/docs/switches.html
* https://www.nginx.com/resources/wiki/start/topics/tutorials/commandline/
